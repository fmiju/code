#include <SDL.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "configure.h"

void move(int servo, float dir);
void stop(int servo);
int isCommandFinished();
void headbang();

int serialPort = 0;
int position[6];

int main(int argc, char **argv)
{
	int n;
	serialPort = openPort(argv[1]);
	configure(serialPort);	
	position[0] = 1470;
	position[1] = 1500;
	position[2] = 1500;
	position[3] = 1480;
	position[4] = position[5] = 1500;
	
	char* stopAll = "#0 P0 #1 P0 #2 P0 #3 P0 #4 P0 #5 P0\r";
	
	if(!strcmp(argv[2], "terminal"))	
	{

	char c[100];
	printf("==== Terminal ready ====\n");	
	do
	{	
		gets(c);
		n = write(serialPort, c, strlen(c));
		write(serialPort, "\r", 1);
		while(!isCommandFinished()) {}; // wait until the command is executed
		//printf("bytes written: %d\n", n); 
	}
	while(strcmp(c, "q") && strlen(c) > 1);// while c != "q"
	}


	else if(!strcmp(argv[2], "keys"))
	{

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("Unable to initialize SDL: %s\n", SDL_GetError());
		return 1;
	}
 
	if(!SDL_SetVideoMode(320, 240, 0, SDL_ANYFORMAT))
	{
		printf("Unable to set video mode: %s\n", SDL_GetError());
		return 1;
	}

	Uint8 *keystate = SDL_GetKeyState(NULL);	
	SDL_Event event;
	
	int run = 1;
	while(run)
	{
		if(SDL_PollEvent(&event)) 
		{
			switch(event.type){
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym)
					{
						case SDLK_l:
							printf("l down\n");
							move(4, -1);
      							break;
						case SDLK_j:
							printf("j down\n");
							move(4, 1);
							break;

						case SDLK_i:
							printf("i down\n");
							move(2, 1);
      							break;
						case SDLK_k:	
							printf("k down\n");
							move(2, -1);
							break;

						case SDLK_d:
							printf("d down\n");
							move(0, -1.5);
      							break;
						case SDLK_a:
							printf("a down\n");
							move(0, 1);
							break;
						
						case SDLK_w:
							printf("w down\n");
							move(1, 1);
      							break;
						case SDLK_s:	
							printf("s down\n");
							move(1, -1);
							break;
	
						case SDLK_h:
							printf("h down\n");
							move(5, -1);
      							break;
						case SDLK_f:
							printf("f down\n");
							move(5, 1);
							break;
						
						case SDLK_t:
							printf("t down\n");
							move(3, 1);
      							break;
						case SDLK_g:	
							printf("g down\n");
							move(3, -2);
							break;


						case SDLK_b:
							printf("b down\n");
							headbang();
							break;

						default:
							printf("other key press detected\n");
							break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.sym)
					{
						case SDLK_l:
							printf("l up\n");
							break;
						case SDLK_j:	
							printf("j up\n");
							break;

						case SDLK_i:
							printf("i up\n");
							stop(2);
      							break;
						case SDLK_k:	
							printf("k up\n");
							stop(2);
							break;

						case SDLK_a:
							printf("a up\n");
							stop(0);
							break;
						case SDLK_d:	
							printf("d up\n");
							stop(0);
							break;

						case SDLK_w:
							printf("w up\n");
							stop(1);
      							break;
						case SDLK_s:	
							printf("s up\n");
							stop(1);
							break;

						case SDLK_h:
							printf("h up\n");
      							break;
						case SDLK_f:	
							printf("f up\n");
							break;

						case SDLK_t:
							printf("t up\n");
							stop(3);
							break;
						case SDLK_g:	
							printf("g up\n");
							stop(3);
							break;

						default:
							printf("other key released\n");
							break;
					}
					break;
				case SDL_QUIT:
					run = 0;
					break;
			}	
		}
	}
	}
	
	n = write(serialPort, stopAll, strlen(stopAll));	
	close(serialPort);
	printf("Port closed\n");
	SDL_Quit();
	return 0;
}


// servo number, dir = 1 or -1
void move(int servo, float dir)
{	
	char str[100];
	int pos;
	if(servo < 4)
	{
		pos = position[servo] + dir*30;
		printf("pos: %d\n", pos);
	}
	else
	{
		position[servo] += dir*1100;
		pos = position[servo];
	}

	snprintf(str, 100, "#%d P%d\r", servo, pos);
	write(serialPort, str, strlen(str));
}

void stop(int servo)
{
	char str[20];
	snprintf(str, 20, "#%d P%d\r", servo, position[servo]);
	write(serialPort, str, strlen(str));
}

int isCommandFinished()
{
	char resp[1];
	write(serialPort, "Q\r", 2);
	
	while(read(serialPort, resp, 1) <= 0)
	{
	}

	switch(resp[0])
	{
		case '.':
			return 1;
			break;
		case '+':
			return 0;
			break;
		default:
			return -1;
			break;
	}
}

void headbang()
{
	char cmd[50];	

	snprintf(cmd, 50, "#2 P%d #5 P1000 T1000\r", position[2]+20);	
	
	write(serialPort, cmd, strlen(cmd));
	
	while(!isCommandFinished()) {}

	stop(2);
	stop(5);
	for(int i = 0; i < 5; i++)
	{
		snprintf(cmd, 50, "#3 P%d T500\r", position[3]+100);
		while(!isCommandFinished()) {}
		write(serialPort, cmd, strlen(cmd));
		printf("forward\n");
		
		snprintf(cmd, 50, "#3 P%d T500\r", position[3]-100);
		while(!isCommandFinished()) {}
		write(serialPort, cmd, strlen(cmd));
		printf("backward\n");
	}
	
	while(!isCommandFinished) {}
	stop(3);
}

