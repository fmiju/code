#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <time.h>

struct termios config;
 
 /*
  * Returns the file descriptor on success or -1 on error.
  */

int openPort(char *port)
{
   int fd; /* File descriptor for the port */
   fd = open(port, O_RDWR | O_NDELAY | O_FSYNC);
   if (fd == -1)
   {
    /*
     * Could not open the port.
     */
     fprintf(stderr, "open_port: Unable to open /dev/ttyUSB0 - %s\n",
             strerror(errno));
   }
	else 
		printf("Serial port opened\n");

   return (fd);
}


void configure(int fd)
{
if(!isatty(fd)) { printf("%d is not a tty\n", fd); return; }
if(tcgetattr(fd, &config) < 0) { /*... error handling ...*/ }
//
// Input flags - Turn off input processing
// convert break to null byte, no CR to NL translation,
// no NL to CR translation, don't mark parity errors or breaks
// no input parity check, don't strip high bit off,
// no XON/XOFF software flow control
//
config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                    INLCR | PARMRK | INPCK | ISTRIP | IXON);
//
// Output flags - Turn off output processing
// no CR to NL translation, no NL to CR-NL translation,
// no NL to CR translation, no column 0 CR suppression,
// no Ctrl-D suppression, no fill characters, no case mapping,
// no local output processing
//
// config.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
//                     ONOCR | ONOEOT| OFILL | OLCUC | OPOST);
config.c_oflag = 0;
//
// No line processing:
// echo off, echo newline off, canonical mode off, 
// extended input processing off, signal chars off
//
config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
//
// Turn off character processing
// clear current char size mask, no parity checking,
// no output processing, force 8 bit input
//
config.c_cflag &= ~(CSIZE | PARENB);
config.c_cflag |= CS8;
//
// One input byte is enough to return from read()
// Inter-character timer off
//
config.c_cc[VMIN]  = 1;
config.c_cc[VTIME] = 0;
//
// Communication speed (simple version, using the predefined
// constants)

printf("Baud rates: in %d, out %d\nParameter given: %d\n", cfgetispeed(&config), cfgetospeed(&config), B115200);


if(cfsetispeed(&config, B115200) < 0 || cfsetospeed(&config, B115200) < 0) {
	printf("Error while setting baud rates\n");
}
else printf("Baud rates set\n");
//
// Finally, apply the configuration
//
if(tcsetattr(fd, TCSAFLUSH, &config) < 0) { printf("Error while applying settings\n"); }
	else printf("Port setup finished\nBaud rates: in %d, out %d\n", cfgetispeed(&config), cfgetospeed(&config));

printf("Config done\n");
}
